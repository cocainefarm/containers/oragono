ARG ORAGONO_VERSION=master
ARG BASE="alpine:3.11"

## build Oragono
FROM golang:rc-alpine AS build-env

ENV ORAGONO_VERSION=$ORAGONO_VERSION

RUN apk add --no-cache git make curl

# get oragono
RUN mkdir -p /go/src/github.com/oragono && \
    git clone \
        --branch $ORAGONO_VERSION \
        --single-branch --recurse-submodules \
        https://github.com/oragono/oragono.git \
        /go/src/github.com/oragono/oragono
WORKDIR /go/src/github.com/oragono/oragono

# compile
RUN make

## run Oragono
FROM $BASE

# metadata
LABEL maintainer="audron@cocaine.farm"
LABEL description="Oragono is a modern, experimental IRC server written in Go"
LABEL version=$ORAGONO_VERSION

EXPOSE 6667/tcp 6697/tcp

# oragono itself
RUN mkdir -p /ircd-bin
COPY --from=build-env /go/bin/oragono /ircd-bin
COPY --from=build-env /go/src/github.com/oragono/oragono/languages /ircd-bin/languages/
COPY --from=build-env /go/src/github.com/oragono/oragono/oragono.yaml /ircd-bin/oragono.yaml
COPY run.sh /ircd-bin/run.sh
RUN chmod +x /ircd-bin/run.sh

# running volume holding config file, db, certs
VOLUME /ircd
WORKDIR /ircd

# default motd
COPY --from=build-env /go/src/github.com/oragono/oragono/oragono.motd /ircd/oragono.motd

# launch
ENTRYPOINT ["/ircd-bin/run.sh"]
